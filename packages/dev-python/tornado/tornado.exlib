# Copyright 2013 Jorge Aparicio
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ]

export_exlib_phases src_test

SUMMARY="Python web framework and asynchronous networking library"
DESCRIPTION="
Tornado is a Python web framework and asynchronous networking library, originally developed at
FriendFeed. By using non-blocking network I/O, Tornado can scale to tens of thousands of open
connections, making it ideal for long polling, WebSockets, and other applications that require a
long-lived connection to each user.
"
HOMEPAGE+=" http://www.tornadoweb.org"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        python_abis:2.7? (
            dev-python/backports_abc[>=0.4][python_abis:2.7]
            dev-python/singledispatch[python_abis:2.7]
        )
        python_abis:3.4? (
            dev-python/backports_abc[>=0.4][python_abis:3.4]
        )
    test:
        dev-python/pycurl[python_abis:*(-)?]
        net-twisted/Twisted[python_abis:*(-)?]
    suggestion:
        dev-python/pycurl[python_abis:*(-)?] [[ description = [ For Tornado's httpclient ] ]]
        net-twisted/Twisted[python_abis:*(-)?] [[ description = [ To run applications written for
            Twisted in a Tornado application ] ]]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # remove tests that require internet connection
    edo sed \
        -e '/tornado.test.iostream_test/d' \
        -e '/tornado.test.tcpclient_test/d' \
        -e '/tornado.test.netutil_test/d' \
        -e '/tornado.test.web_test/d' \
        -e '/tornado.test.wsgi_test/d' \
        -i tornado/test/runtests.py
}

tornado_src_test() {
    local local_port_range="32768-60999"
    local whitelist=(
        "--connect LOCAL@${local_port_range}"
        "unix:${TEMP}/tmp*/test.sock"
        "unix:${TEMP}/tmp*/tmp*.sock"
        "unix:${TEMP}/tmp*/tornado.test.twisted_test/*/*/*/temp"
    )

    for addr in "${whitelist[@]}"; do
        esandbox allow_net $addr
    done
    setup-py_src_test
    for addr in "${whitelist[@]}"; do
        esandbox disallow_net $addr
    done
}

test_one_multibuild() {
    # Don't run pytest directly because it's easier to disable tests
    # when using tornado's own tool.
    PYTHONPATH=$(ls -d build/lib*) edo ${PYTHON} -m tornado.test.runtests --verbose
}

