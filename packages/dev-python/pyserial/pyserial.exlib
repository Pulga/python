SUMMARY="Python Serial Port Extension"
HOMEPAGE="http://pyserial.wiki.sourceforge.net/pySerial"
LICENCES="BSD-3"
DESCRIPTION="
This module encapsulates the access for the serial port. It provides backends
for Python running on Windows, Linux, BSD (possibly any POSIX compliant system),
Jython and IronPython (.NET and Mono). The module named 'serial' automatically
selects the appropriate backend.
Features include:
* Same class based interface on all supported platforms.
* Access to the port settings through Python 2.2+ properties.
* Port numbering starts at zero, no need to know the port name in the user
  program.
* Port string (device name) can be specified if access through numbering is
  inappropriate.
* Support for different bytesizes, stopbits, parity and flow control with
  RTS/CTS and/or Xon/Xoff.
* Working with or without receive timeout.
* File like API with 'read' and 'write' ('readline' etc. also supported)
* The files in this package are 100% pure Python. They depend on non standard
  but common packages on Windows (pywin32) and Jython (JavaComm). POSIX (Linux,
  BSD) uses only modules from the standard Python distribution).
* The port is set up for binary transmission. No NULL byte stripping, CR-LF
  translation etc. (which are many times enabled for POSIX.) This makes this
  module universally useful.
"
BUGS_TO="alip@exherbo.org"
REMOTE_IDS=""
UPSTREAM_CHANGELOG="http://pyserial.svn.sourceforge.net/viewvc/pyserial/trunk/pyserial/CHANGES.txt"
UPSTREAM_RELEASE_NOTES=""

